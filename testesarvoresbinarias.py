# testes

from linkedbinarytrees import LinkedBinaryTree


def ab0():
    return LinkedBinaryTree("Z")


def ab1():
    n3 = LinkedBinaryTree("C")
    n4 = LinkedBinaryTree("E")
    n5 = LinkedBinaryTree("F")
    n2 = LinkedBinaryTree("D")
    n1 = LinkedBinaryTree("B", None, n3)
    n2 = LinkedBinaryTree("D", n4, n5)
    ab = LinkedBinaryTree("A", n1, n2)
    return ab


def ab2():
    t1 = LinkedBinaryTree("D")
    t2 = LinkedBinaryTree("E")
    t3 = LinkedBinaryTree("F")
    t4 = LinkedBinaryTree("B", t1, t2)
    t5 = LinkedBinaryTree("C", t3, None)
    t = LinkedBinaryTree("A", t4, t5)
    return t


def tests1(ab):
    print("Vazia? ", ab.is_empty())
    print("len ", len(ab))
    print(ab)
    print("apagar")
    ab.clear()
    print("Vazia? ", ab.is_empty())
    print("len ", len(ab))
    new_ab = ab
    print("Travessia prefixa")
    new_ab.prefix()
    print("Travessia infixa")
    new_ab.infix()
    print("Travessia sufixa")
    new_ab.sufix()
    print("Travessia largura")
    new_ab.breath_first()
    print()


def tests2(ab: LinkedBinaryTree):
    print("Altura", ab.height(), )
    print("Folha?", ab.is_leaf())
    print("Folhas", ab.num_leaves())
    for node in ab:
        print(node)
    print("A" in ab)
    print("Z" in ab)


if __name__ == '__main__':
    tests1(ab0())
    tests2(ab0())
    tests1(ab1())
    tests2(ab1())
    tests1(ab2())
    tests2(ab2())
