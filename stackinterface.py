class StackInterface:
    """ Interface para o TDA Stack """

    def __init__(self, source_collection = None):
        """Define o estado inicial de self com sourceCollection """
        pass

    # metodos gerais de coleção
    
    def is_empty(self):
        """ Retorna True se len(self) é 0, senão False """
        return True

    def __len__(self):
        """ Retorna o numero de elementos do stack """
        return 0

    def __str__(self):
        """ Retorna a representação em string de self """
        return ""

    def clear(self):
        """ Torna self vazio"""
        pass

    def __iter__(self):
        """ Suporta a iteração sobre self """
        return None

    # metodos específicos da pilha

    def peek(self):
        """ Retorna o item que está no topo de self.
             precondição: self não é vazio."""
        return None

    def push(self, item):
        """ Sobrepoe item a self
            pos-condição: item foi sobreposto a self """
        pass

    def pop(self):
        """ Remove elemento do topo de self e retorna esse elemento.
             precondição: self não é vazio.
             pos-condição: topo foi removido de self """
        return None
