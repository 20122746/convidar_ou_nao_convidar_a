from MapEntry import *


# Implementation of Map ADT using a Python list
class Map:

    def __init__(self):
        pass

    # Returns the number of entries in the map.py
    def __len__(self):
        pass

    # Find the index position of a key. If the key is not found, None is returned
    def find_position(self, key):
        pass
    
    # Determines if the map.py contains the given key
    def __contains__(self, key):
        pass

    # Adds a new entry to the map.py if the key does exist;
    # otherwise, the new value replaces the current value associated with the key
    def add(self, key, value):
        pass

    # Returns the value associated with the key
    def value_of(self, key):
        pass
    
    # Removes the entry associated with the key
    def remove(self, key):
        pass

    # Returns an iterator for traversing the keys in the map.py
    def __iter__(self):
        pass
    
    def print_map(self):
        pass
