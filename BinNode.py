class BinNode:
    """Representa um no ligado"""
    def __init__(self, data):
        self._data = data
        self._left = None
        self._right = None

    def __str__(self):
        return str(self._data)
