class BinaryTreeInterface:
    """Interface para árvore binária."""
    
    def __init__(self, root, left = None, right = None):
        """define o estado inicial de self"""
        pass

    # metodos gerais de coleção
    
    def is_empty(self):
        """retorna True se len(self) == 0, senão False"""
        return True

    def __len__(self):
        """retorna o numero de elementos de self"""

    def __str__(self):
        """retorna a representação em string de self"""
        return ""

    def __iter__(self):
        """suporta a iteração prefixa sobre self"""
        return None

    def __contains__(self,item):
        """Retorna True se o item está em self, caso contrário, retorna False"""
        return False

    def clear(self):
        """torna o self vazio"""
        pass

    #metodos especificos da árvore binária

    def root(self):
        """devolve valor da raiz da árvore
        precondição: self não é vazio """
        return None

    def left(self):
        """devolve subárvore esquerda
        precondição: self tem subárvore direita """
        return None

    def right(self):
        """Devolve a subarvore direita
        precondição: self tem subárvore direita"""
        return None

    def prefix(self):
        """Efetua a travessia prefixa de self"""
        pass

    def infix(self):
        """Efetua a travessia infixa de self"""
        pass

    def sufix(self):
        """Efetua a travessia infixa de self"""
        pass

    def breath_first(self):
        """ Efetua a travessia em largura de self"""
        return None

    def is_leaf(self):
        """ Verifica se self é folha"""
        return None

    def height(self):
        """Devolve a altura de self (número de níveis)"""
        return 0

    def num_leaves(self):
        """Devolve o número de folhas de self"""
        return 0
