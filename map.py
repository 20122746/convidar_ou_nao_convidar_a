from MapEntry import *


# Implementation of Map ADT using a Python list
class Map:

    def __init__(self):
        self._list = []

    # Returns the number of entries in the map.py
    def __len__(self):
        return len(self._list)

    # Find the index position of a key. If the key is not found, None is returned
    def find_position(self, key):
        keys = list(map(lambda x: x.key, self._list))
        if key in keys:
            result = keys.index(key)
        else:
            result = None
        return result

    # Determines if the map.py contains the given key
    def __contains__(self, key):
        return self.find_position(key) is not None

    # Adds a new entry to the map.py if the key does exist;
    # otherwise, the new value replaces the current value associated with the key
    def add(self, key, value):
        p = self.find_position(key)
        if p is not None:
            me: MapEntry = self._list[p]
            me.value = value
        else:
            me = MapEntry(key, value)
            self._list.append(me)

    # Returns the value associated with the key
    def value_of(self, key):
        p = self.find_position(key)
        if p is not None:
            result = self._list[p].value
        else:
            result = None
        return result

    # Removes the entry associated with the key
    def remove(self, key):
        p = self.find_position(key)
        if p is not None:
            self._list.pop(p)

    # Returns an iterator for traversing the keys in the map.py
    def __iter__(self):
        return iter(self._list)

    def print_map(self):
        for entry in self._list:
            print(str(entry))
