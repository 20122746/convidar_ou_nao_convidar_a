"""
File: LinkedQueue.py
Author: Helia Guerra
"""

from node import Node

class LinkedQueue:
    """ Implementação de queue baseada na estrutura de dados ligada, recorrendo
    a duas referencias: front e rear """
    
        
    def __init__(self, sourceCollection=None):
        """ Define o estado inicial de self com sourceCollection """
        self._front=None 
        self._rear=None 
        self._size=0
        if sourceCollection:
            for item in sourceCollection:
                self.add(item)

    # metodos gerais de coleção
    
    def is_empty(self):
        """ Retorna True se len(self) é 0, senão False """
        return self._front==None

    def __len__(self):
        """ Retorna o numero de elementos da queue """
        return self._size

    def __str__(self):
        """ Retorna a representação em string de self """
        return "["+" ".join(map(str,self))+"]"

    def clear(self):
        """ Torna o self vazio"""
        self._size=0
        self._front=None
        self._rear=None

    def __iter__(self):
        """ Suporta a iteração sobre self """
        cursor=self._front
        while not cursor is None: 
            yield cursor._data  
            cursor=cursor._next

    # metodos especificos da fila

    def peek(self):
        """ Retorna o item que está na frente de self.
             precondição: self não é vazio."""
        if self.is_empty():
            raise KeyError(" queue is empty")
        else:
            return self._front._data

    def add(self, item):
        """ Acrescenta item a self no fim
            pos-condição: item foi acrescentado a self """
        novo=Node(item)
        if not self.is_empty():
            self._rear._next=novo
            self._rear=novo
        else:
            self._rear=novo
            self._front=self._rear
        self._size+=1

    def pop(self):
        """ Remove elemento do topo de self e retorna esse elemento.
             precondição: self não é vazio.
             pos-condição: item foi removido de self """
        front=self._front._data
        self._front=self._front._next
        if self._front==None:
            self._rear=None
        self._size-=1
        return front
