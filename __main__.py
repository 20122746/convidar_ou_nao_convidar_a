import csv
from map import Map
from linkedbinarytrees import LinkedBinaryTree

CONVIDAR = "Convidar"
NAO_CONVIDAR = "Não convidar"


def ler_mapa() -> Map:
    m = Map()
    with open("data.csv", mode="r") as in_file:
        reader = csv.reader(in_file, delimiter=";")
        for entry in reader:
            m.add(entry[0], {"NOME": entry[1], "SS": entry[2], "IDADE": entry[3], "SEXO": entry[4], "EST.LAB.": entry[5],
                             "RENDIMENTO": entry[6], "GPA": entry[7], "DECISAO": entry[8]})

    return m


def criar_arvore_decisao(mapa: Map) -> LinkedBinaryTree:
    # ramo esquerdo, leva para outra decisão ou não convidar
    # ramo da direita, leva para outra decisou ou convidar

    arv_convidar = LinkedBinaryTree(CONVIDAR, None, None)
    arv_nao_convidar = LinkedBinaryTree(NAO_CONVIDAR, None, None)

    arv_gpa = LinkedBinaryTree("GPA>=3", arv_nao_convidar, arv_convidar)
    arv_rend = LinkedBinaryTree("RENDIMENTO>=30000", arv_nao_convidar, arv_convidar)
    arv_idade = LinkedBinaryTree("IDADE>=21", arv_nao_convidar, arv_gpa)

    est_lab = LinkedBinaryTree("EST.LAB.=""EMP""", arv_idade, arv_rend)
    return est_lab


# analisar o perfil em questão usando a arvore de decisão binária
def analisar_perfil(arv: LinkedBinaryTree, perfil: dict) -> str:
    node = arv
    return ""



def main():
    mapa: Map = ler_mapa()
    arv_decisao = criar_arvore_decisao(mapa)
    print(arv_decisao)