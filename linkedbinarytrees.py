from LinkedQueue import LinkedQueue


class LinkedBinaryTree:
    """Implementacao para arvore binaria como estrutura ligada"""

    def __init__(self, root, left=None, right=None):
        """define o estado inicial de self"""
        self._root = root
        self._left = left
        self._right = right

    # metodos gerais de coleção

    def is_empty(self):
        """retorna True se len(self) == 0, senão False"""
        return self._root is None

    def __len__(self):
        """retorna o numero de elementos de self"""
        if self._root is None:
            return 0
        elif self._left is None and self._right is None:
            return 1
        elif self._left is None:
            return 1 + len(self._right)
        elif self._right is None:
            return 1 + len(self._left)
        else:
            return 1 + len(self._left) + len(self._right)

    def __str__(self, level=0):
        """retorna a representação em string de self"""
        if self.is_empty():
            return ''
        else:
            ret = '\t' * level + str(self._root) + '\n'
            for child in [self._left,self._right]:
                if child is not None:
                    ret += child.__str__(level+1)
            return ret

    def __iter__(self):
        """suporta a iteração prefixa sobre self"""
        def generator():
            if not self.is_empty():
                if self._left is not None:
                    for leaf in self._left:
                        yield leaf
                yield self._root
                if self._right is not None:
                    for leaf in self._right:
                        yield leaf

        return generator()

    def __contains__(self, item):
        """Retorna True se o item está em self, caso contrário, retorna False"""
        if self.is_empty():
            return False
        elif self._root == item:
            return True
        else:
            if self._left is not None and self._right is not None:
                  return item in self._left or item in self._right
            elif self._left is not None:
                return item in self._left
            elif self._right is not None:
                return item in self._right

    def clear(self):
        """torna o self vazio"""
        self._root = None
        self._left = None
        self._right = None

    #metodos especificos da árvore binária

    def root(self):
        """devolve valor da raiz da árvore
        precondição: self não é vazio """
        if self._root is not None:
            return self._root

    def left(self):
        """devolve subárvore esquerda
        precondição: self tem subárvore direita """
        if self._left is not None:
            return self._left

    def right(self):
        """Devolve a subarvore direita
        precondição: self tem subárvore direita"""
        if self._right is not None:
            return self._right

    def prefix(self):
        """Efetua a travessia prefixa de self"""
        if self._root is not None:
            print(self._root)
            if self._left is not None:
                self._left.prefix()
            if self._right is not None:
                self._right.prefix()

    def infix(self):
        """Efetua a travessia infixa de self"""
        if self._root is not None:
            if self._left is not None:
                self._left.prefix()
            print(self._root)
            if self._right is not None:
                self._right.prefix()

    def sufix(self):
        """Efetua a travessia infixa de self"""
        if self._root is not None:
            if self._left is not None:
                self._left.prefix()
            if self._right is not None:
                self._right.prefix()
            print(self._root)

    def breath_first(self):
        """ Efetua a travessia em largura de self"""
        if not self.is_empty():
            q = LinkedQueue()
            q.add(self)
            while not q.is_empty():
                print(q.peek()._root, end="\t")
                if q.peek()._left is not None:
                    q.add(q.peek()._left)
                if q.peek()._right is not None:
                    q.add(q.peek()._right)
                q.pop()

    def is_leaf(self):
        """ Verifica se self é folha"""
        if not self.is_empty():
            return self._left == None and self._right == None

    def height(self):
        """Devolve a altura de self (número de níveis)"""
        if self.is_empty():
            return 0
        elif self.is_leaf():
            return 1
        else:
            if self._left is not None and self._right is not None:
                return 1 + max(self._left.height(), self._right.height())
            elif self._left is None:
                return 1 + self._right.height()
            else:
                return 1 + self._left.height()

    def num_leaves(self):
        """Devolve o número de folhas de self"""
        if self.is_empty():
            return 0
        elif self._left is None and self._right is None:
            return 1
        elif self._left is None:
            return self._right.num_leaves()
        elif self._right is None:
            return self._left.num_leaves()
        else:
            return self._left.num_leaves()+self._right.num_leaves()

